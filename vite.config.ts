import path from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/index.ts'),
            name: 'ftrack',
            fileName(format: string) {
                return `ftrack.${format}.js`
            },
        },
    },
})
