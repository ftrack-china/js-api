export default function normalizeString(value: string) {
    try {
        return value.normalize()
    } catch {
        return value
    }
}