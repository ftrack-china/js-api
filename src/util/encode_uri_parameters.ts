export default function encodeUriParameters(data: Record<string, string>) {
    const params = new URLSearchParams(data)

    return params.toString()
}
