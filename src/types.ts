export type RecordObject = Record<string, any>

export type Entity = RecordObject & {
    __entity_type__: string
}

export interface ServerInformation {
    /**
     * The server version in the format MAJOR.MINOR.PATCH.BUILD. 
     */
    version: string

    /**
     * An MD5 message digest of the Query schemas response.
     * Can be used as an optimisation to avoid unnecessary Query schemas operation
     * if schemas cached locally. 
     */
    schema_hash?: string

    /**
     * If timezones are enabled on server. If timezones are enabled,
     * all datetime data is assumed to be in UTC and otherwise in the servers local time. 
     */
    is_timezone_support_enabled?: boolean

    /**
     * Storage scenario configuration for automatic setup of API Location plugin. 
     */
    storage_scenario?: RecordObject
}

export interface Schema {
    properties: RecordObject
    default_projections: string[]
    computed?: string[]
    required: string[]
    immutable: string[]
    alias_for?: string | RecordObject
    type: string
    id: string
    system_projections?: string[]
    primary_key: string[]
}

export interface QueryResponse<T extends Entity> {
    action: 'query'
    data: T[]
    metadata: RecordObject
}

export interface CreateResponse<T extends Entity> {
    action: 'create'
    data: T
}

export interface UpdateResponse<T extends Entity> {
    action: 'update'
    data: T
}

export interface DeleteResponse {
    action: 'delete'
    data: boolean
}

export interface DatetimeObject {
    __type__: 'datetime'
    value: string
}
