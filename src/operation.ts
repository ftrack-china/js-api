import { RecordObject } from './types'

export type Operation = RecordObject & {
    action: string
}

export interface CreateOperation extends Operation {
    action: 'create'
    entity_type: string
    entity_data: object
}

export interface QueryOperation extends Operation {
    action: 'query'
    expression: string
}

export interface UpdateOperation extends Operation {
    action: 'update'
    entity_type: string
    entity_key: string[]
    entity_data: object
}

export interface DeleteOperation extends Operation {
    action: 'delete'
    entity_type: string
    entity_key: string[]
}

function buildEntityData(type: string, data: RecordObject) {
    return Object.assign({}, data, { __entity_type__: type})
}

export function createOperation(type: string, data: RecordObject): CreateOperation {
    const entity_data = buildEntityData(type, data)

    return {
        action: 'create',
        entity_type: type,
        entity_data,
    }
}

export function queryOperation(expression: string): QueryOperation {
    return {
        action: 'query',
        expression,
    }
}

export function updateOperation(type: string, keys: string[], data: RecordObject): UpdateOperation {
    const entity_data = buildEntityData(type, data)

    return {
        action: 'update',
        entity_type: type,
        entity_key: keys,
        entity_data,
    }
}

export function deleteOperation(type: string, keys: string[]): DeleteOperation {
    return {
        action: 'delete',
        entity_type: type,
        entity_key: keys,
    }
}

export default {
    query: queryOperation,
    create: createOperation,
    update: updateOperation,
    delete: deleteOperation,
}
