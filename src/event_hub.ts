import { v4 as uuidV4 } from 'uuid'
import io from './socket.io'
import { Event, EventData, EventSource } from './event'
import {
    EventServerConnectionTimeoutError,
    EventServerReplyTimeoutError,
    NotUniqueError,
} from './error'
import encodeUriParameters from './util/encode_uri_parameters'
import debug from 'debug'

const logger = debug('ftrack_api:EventHub')

export interface EventHubOptions {
    /**
     * Application identifier, added to event source
     */
    applicationId?: string
}

export type SubscriberMetadata = Partial<EventSource>

export type PublishCallback = () => void
export type ReplyCallback = (event: EventData) => void

export interface Subscriber {
    subscription: string
    callback: ReplyCallback
    metadata: SubscriberMetadata
}

export interface PublishOptions {
    /**
     * Function to be invoked when a reply is received.
     */
    onReply?: ReplyCallback

    /**
     * Timeout in seconds
     */
    timeout?: number
}

export class EventHub {
    private _id: string
    private _serverUrl: string
    private _apiUser: string
    private _apiKey: string
    private _applicationId: string

    private _socketIo: any
    private _replyCallbacks: Record<string, ReplyCallback>
    private _unsentEvents: PublishCallback[]
    private _subscribers: Subscriber[]

    /**
     * Construct EventHub instance with API credentials.
     */
    constructor(serverUrl: string, apiUser: string, apiKey: string, options: EventHubOptions = {}) {
        const { applicationId = 'ftrack.api.javascript' } = options

        this._applicationId = applicationId
        this._apiUser = apiUser
        this._apiKey = apiKey

        // Socket.IO guesses port based on the current web page instead of
        // the server URL, which causes issues when using the API on a page
        // hosted on a non-standard port.
        const url = new URL(serverUrl)
        if (url.port !== '') {
            this._serverUrl = serverUrl
        } else {
            const port = serverUrl.startsWith('https') ? '443' : '80'
            this._serverUrl = `${serverUrl}:${port}`
        }

        this._id = uuidV4()
        this._replyCallbacks = {}
        this._unsentEvents = []
        this._subscribers = []
    }

    /**
     * Connect to the event server
     */
    connect() {
        this._socketIo = io.connect(this._serverUrl, {
            'max reconnection attempts': Infinity,
            'reconnection limit': 10000,
            'reconnection delay': 5000,
            transports: ['websocket'],
            query: encodeUriParameters({
                api_user: this._apiUser,
                api_key: this._apiKey,
            }),
        })

        this._socketIo.on('connect', this._onSocketConnected.bind(this))
        this._socketIo.on('ftrack.event', this._handle.bind(this))
    }

    /**
     * Return true if connected to event server
     */
    isConnected(): boolean {
        return this._socketIo && this._socketIo.socket.connected
    }

    /**
     * Handle on connect event.
     *
     * Subscribe to replies and send any queued events.
     */
    private _onSocketConnected() {
        logger('Connected to event server.')

        try {
            this.subscribe('topic=ftrack.meta.reply', this._handleReply.bind(this), { id: this._id })
        } catch (error) {
            if (error instanceof NotUniqueError) {
                logger('Already subscribed to replies.')
            } else {
                throw error
            }
        }

        // Now resubscribe any existing stored subscribers. This can happen when
        // reconnecting automatically for example.
        for (const subscriber of this._subscribers) {
            this._notifyServerAboutSubscriber(subscriber)
        }

        // Run any publish callbacks.
        const callbacks = this._unsentEvents
        if (callbacks.length) {
            this._unsentEvents = []
            logger('Publishing %d unsent events.', callbacks.length)
            for (const callback of callbacks) {
                this._runWhenConnected(callback)
            }
        }
    }

    /**
     * Publish event and return promise resolved with event id when event has
     * been sent.
     *
     * If *onReply* is specified, it will be invoked when any replies are
     * received.
     *
     * If timeout is non-zero, the promise will be rejected if the event is not
     * sent before the timeout is reached. Should be specified as seconds and
     * will default to 10.
     */
    publish(event: Event, options: PublishOptions = {}) {
        const { onReply = null, timeout = 10 } = options

        event.addSource(
            {
                id: this._id,
                applicationId: this._applicationId,
                user: {
                    username: this._apiUser,
                },
            }
        )

        // Copy event data to avoid mutations before async callbacks.
        const eventData = Object.assign({}, event.getData())
        const eventId = eventData.id

        const onConnected = new Promise<void>((resolve, reject) => {
            this._runWhenConnected(resolve)

            if (timeout) {
                setTimeout(
                    () => {
                        const error = new EventServerConnectionTimeoutError(
                            'Unable to connect to event server within timeout.'
                        )
                        reject(error)
                    },
                    timeout * 1000
                )
            }
        })

        const onPublish = onConnected.then(() => {
            if (onReply) {
                this._replyCallbacks[eventId] = onReply
            }

            logger('Publishing event.', eventData)
            this._socketIo.emit('ftrack.event', eventData)
            return Promise.resolve(eventId)
        })

        return onPublish
    }

    /**
     * Publish event and wait for a single reply.
     *
     * Returns promise resolved with reply event if received within timeout.
     * 
     * @param event Event instance to publish
     */
    publishAndWaitForReply(event: Event, options: Omit<PublishOptions, 'onReply'> = {}) {
        const eventId = event.getData().id
        const { timeout = 30 } = options

        const response = new Promise((resolve, reject) => {
            const onReply = (replyEvent: EventData) => {
                resolve(replyEvent)
                this._removeReplyCallback(eventId)
            }
            this.publish(event, { timeout, onReply })

            if (timeout) {
                setTimeout(() => {
                    const error = new EventServerReplyTimeoutError(
                        'No reply event received within timeout.'
                    )
                    reject(error)
                    this._removeReplyCallback(eventId)
                }, timeout * 1000)
            }
        })

        return response
    }

    private _removeReplyCallback(eventId: string) {
        if (this._replyCallbacks[eventId]) {
            delete this._replyCallbacks[eventId]
        }
    }

    /**
     * Run *callback* if event hub is connected to server.
     */
    private _runWhenConnected(callback: PublishCallback) {
        if (!this.isConnected()) {
            debug('Event hub is not connected, event is delayed.')
            this._unsentEvents.push(callback)

            // Force reconnect socket if not automatically reconnected. This
            // happens for example in Adobe After Effects when rendering a
            // sequence takes longer than ~30s and the JS thread is blocked.
            this._socketIo.socket.reconnect()
        } else {
            callback()
        }
    }

    /**
     * Register to *subscription* events.
     *
     * @param subscription Expression to subscribe on. Currently,
     * only "topic=value" expressions are supported.
     * @param callback Function to be called when an event matching the subscription is returned.
     * @param metadata Optional information about subscriber.
     * @return Subscriber ID.
     */
    subscribe(subscription: string, callback: ReplyCallback, metadata: SubscriberMetadata = {}) {
        const subscriber = this._addSubscriber(
            subscription, callback, metadata
        )
        this._notifyServerAboutSubscriber(subscriber)
        return subscriber.metadata.id as string
    }

    /**
     * Return topic from *subscription* expression.
     *
     * Raises an error if expression is in an unsupported format. Currently,
     * only expressions on the format topic=value is supported.
     *
     * @param subscription expression
     * @return topic
     */
    private _getExpressionTopic(subscription: string) {
        // retreive the value of a topic on the format "topic=value"
        const regex = new RegExp('^topic[ ]?=[ \'"]?([\\w-,./*@+]+)[\'"]?$')
        const matches = subscription.trim().match(regex)
        if (matches && matches.length === 2) {
            return matches[1]
        }
        throw new Error(
            'Only subscriptions on the format "topic=value" are supported.'
        )
    }

    /**
     * Add subscriber locally.
     *
     * Throws an NotUniqueError if a subscriber with
     * the same identifier already exists.
     *
     * @param subscription expression
     * @param callback Function to be called when an event is received.
     * @param metadata Optional information about subscriber.
     * @return subscriber information.
     */
    private _addSubscriber(subscription: string, callback: ReplyCallback, metadata: SubscriberMetadata) {
        // Ensure subscription is on supported format.
        // TODO: Remove once subscription parsing is supported.
        this._getExpressionTopic(subscription)

        if (!metadata.id) {
            metadata.id = uuidV4()
        }

        // Check subscriber not already subscribed.
        const existingSubscriber = this.getSubscriberByIdentifier(
            metadata.id
        )

        if (existingSubscriber) {
            throw new NotUniqueError(
                `Subscriber with identifier "${metadata.id}" already exists.`
            )
        }

        const subscriber: Subscriber = {
            subscription,
            callback,
            metadata,
        }
        this._subscribers.push(subscriber)
        return subscriber
    }

    /**
     * Notify server of new *subscriber*.
     * @param subscriber subscriber information
     */
    private _notifyServerAboutSubscriber(subscriber: Subscriber) {
        const subscribeEvent = new Event(
            'ftrack.meta.subscribe',
            {
                subscriber: subscriber.metadata,
                subscription: subscriber.subscription,
            }
        )
        this.publish(subscribeEvent)
    }

    /**
     * Return subscriber with matching *identifier*.
     *
     * Return null if no subscriber with *identifier* found.
     *
     * @param identifier
     */
    getSubscriberByIdentifier(identifier: string) {
        for (const subscriber of this._subscribers.slice()) {
            if (subscriber.metadata.id === identifier) {
                return subscriber
            }
        }
        return null
    }

    /**
     * Return if *subscriber* is interested in *event*.
     *
     * Only expressions on the format topic=value is supported.
     *
     * TODO: Support the full event expression format.
     */
    private _IsSubscriberInterestedIn(subscriber: Subscriber, event: EventData) {
        const topic = this._getExpressionTopic(subscriber.subscription)
        if (topic === event.topic) {
            return true
        }
        return false
    }

    /**
     * Handle Events.
     * @param event Event payload
     */
    private _handle(event: EventData) {
        logger('Event received:', event)

        for (const subscriber of this._subscribers) {
            // TODO: Parse event target and check that it matches subscriber.

            // TODO: Support full expression format as used in Python.
            if (!this._IsSubscriberInterestedIn(subscriber, event)) {
                continue
            }

            let response = null
            try {
                response = subscriber.callback(event)
            } catch (error) {
                logger('Error calling subscriber for event.', error, subscriber, event)
            }

            // Publish reply if response isn't null or undefined.
            if (response != null) {
                this.publishReply(event, response, subscriber.metadata)
            }
        }
    }

    /**
     * Handle reply event.
     * @param event Event payload
     */
    private _handleReply(event: EventData) {
        logger('Reply received:', event)

        const onReplyCallback = this._replyCallbacks[event.inReplyToEvent!]
        if (onReplyCallback) {
            onReplyCallback(event)
        }
    }

    /**
     * Publish reply event.
     * @param sourceEvent Source event payload
     * @param data Response event data
     * @param source Response event source information
     */
    publishReply(sourceEvent: EventData, data: object, source?: EventSource) {
        const replyEvent = new Event(
            'ftrack.meta.reply',
            {
                ...data,
                target: `id=${sourceEvent.source!.id}`,
                inReplyToEvent: sourceEvent.id,
            }
        )

        if (source) {
            replyEvent.addSource(source)
        }
        return this.publish(replyEvent)
    }
}
