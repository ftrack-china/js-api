export abstract class CustomError extends Error {
    name: string = typeof this
    errorCode?: string

    constructor(message?: string, errorCode?: string) {
        super(message)
        this.errorCode = errorCode
    }
}

/**
 * Throw when a unknown server error occurs.
 */
export class ServerError extends CustomError {}

/**
 * Throw when a permission denied error occurs.
 */
export class ServerPermissionDeniedError extends CustomError {}

/**
 * Throw when a validation error occurs.
 */
export class ServerValidationError extends CustomError {}

/**
 * Throw when event reply timeout occurs.
 */
export class EventServerReplyTimeoutError extends CustomError {}

/**
 * Throw when event server connection timeout occurs.
 */
export class EventServerConnectionTimeoutError extends CustomError {}

/**
 * Throw when event server connection timeout occurs.
 */
export class NotUniqueError extends CustomError {}

/**
 * Throw when file upload to event server is aborted or does not succeed.
 */
export class CreateComponentError extends CustomError {}
