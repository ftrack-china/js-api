import dayjs from 'dayjs'
import dayjsUtc from 'dayjs/plugin/utc'
import debug from 'debug'
import { find, forIn, isArray, isPlainObject, isString } from 'lodash-es'
import { v4 as uuidV4 } from 'uuid'
import { SERVER_LOCATION_ID } from './constant'
import { CreateComponentError, ServerError, ServerPermissionDeniedError, ServerValidationError } from './error'
import { EventHub, EventHubOptions } from './event_hub'
import { createOperation, deleteOperation, DeleteOperation, Operation, queryOperation, updateOperation } from './operation'
import type { CreateResponse, DatetimeObject, Entity, QueryResponse, RecordObject, Schema, ServerInformation, UpdateResponse } from './types'
import encodeUriParameters from './util/encode_uri_parameters'
import normalizeString from './util/normalize_string'


const logger = debug('ftrack_api')

const ENCODE_DATETIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss'

dayjs.extend(dayjsUtc)

/**
 * Create component from *file* and add to server location.
 *
 * @param fileName The name of the file.
 * @return Array with [basename, extension] from filename.
 */
function splitFileExtension(fileName: string) {
    let basename = fileName || ''
    let extension = fileName.slice(
        (
            Math.max(
                0, fileName.lastIndexOf('.')
            ) || Infinity
        ) + 1
    ) || ''

    if (extension.length) {
        extension = `.${extension}`
        basename = fileName.slice(0, -1 * extension.length) || ''
    }

    return [basename, extension]
}

export interface SessionOptions {
    /**
     * Automatically connect to event hub.
     */
    autoConnectEventHub?: boolean

    /**
     * List of server information values to retrieve.
     */
    serverInformationValues?: string[]

    /**
     * Options to configure event hub with.
     */
    eventHubOptions?: EventHubOptions

    /**
     * Client token for update events.
     */
    clientToken?: string

    /**
     * API endpoint.
     */
    apiEndpoint?: string
}

export interface CreateComponentOptions {
    onProgress?: (progress: number) => void
    xhr?: XMLHttpRequest
    data?: {
        id?: string
        file_type?: string
        name?: string
        size?: number
    }
    onAborted?: () => void
}

export interface ThumbnailOptions {
    /**
     * The size of the thumbnail. The image will be resized to fit within size x size pixels.
     * Defaults to 300.
     */
    size?: number
}

interface UploadMetadata {
    url: string,
    headers: Record<string, string>
}

export class Session {
    /**
     * Username of ftrack user used by API
     */
    apiUser: string

    /**
     * API Key
     */
    apiKey: string

    /**
     * ftrack server URL
     */
    serverUrl: string


    /**
     * API Endpoint. Specified relative to server URL with leading slash.
     */
    apiEndpoint: string

    /**
     * session event hub
     */
    eventHub: EventHub

    clientToken: string

    /**
     * true if session is initialized
     */
    initialized: boolean

    /**
     * Resolved once session is initialized.
     */
    initializing: Promise<Session>

    serverInformation?: ServerInformation
    schemas?: Schema[]
    serverVersion?: string

    /**
     * Construct Session instance with API credentials.
     */
    constructor(
        serverUrl: string, apiUser: string, apiKey: string, options: SessionOptions = {}
    ) {
        const {
            autoConnectEventHub = false,
            serverInformationValues = null,
            eventHubOptions = {},
            clientToken = null,
            apiEndpoint = '/api',
        } = options

        if (!serverUrl || !apiUser || !apiKey) {
            throw new Error(
                'Invalid arguments, please construct Session with ' +
                '*serverUrl*, *apiUser* and *apiKey*.'
            )
        }

        this.apiUser = apiUser
        this.apiKey = apiKey
        this.serverUrl = serverUrl
        this.apiEndpoint = apiEndpoint
        this.eventHub = new EventHub(serverUrl, apiUser, apiKey, eventHubOptions)

        if (autoConnectEventHub) {
            this.eventHub.connect()
        }

        if (clientToken) {
            this.clientToken = clientToken
        } else {
            this.clientToken = `ftrack-javascript-api--${uuidV4()}`
        }

        // Always include is_timezone_support_enabled as required by API.
        if (
            serverInformationValues &&
            !serverInformationValues.includes('is_timezone_support_enabled')
        ) {
            serverInformationValues.push('is_timezone_support_enabled')
        }

        const operations = [
            { action: 'query_server_information', values: serverInformationValues },
            { action: 'query_schemas' },
        ]

        this.initialized = false

        /**
         * Resolved once session is initialized.
         * @memberof Session
         * @instance
         * @type {Promise}
         */
        this.initializing = this.call<[ServerInformation, Schema[]]>(operations)
            .then(responses => {
                this.serverInformation = responses[0]
                this.schemas = responses[1]
                this.serverVersion = this.serverInformation.version
                this.initialized = true

                return Promise.resolve(this)
            }
        )
    }

    /**
     * Get primary key attributes from schema
     *
     * @return List of primary key attributes.
     */
    getPrimaryKeyAttributes(entityType: string) {
        const schema = find(this.schemas, item => item.id === entityType)
        if (!schema || !schema.primary_key) {
            logger('Primary key could not be found for: %s', entityType)
            return null
        }
        return schema.primary_key
    }

    /**
     * Get identifying key for *entity*
     *
     * @return Identifying key for *entity*
     */
    getIdentifyingKey(entity: Entity) {
        const primaryKeys = this.getPrimaryKeyAttributes(entity.__entity_type__)
        if (primaryKeys) {
            return [
                entity.__entity_type__,
                ...primaryKeys.map(attribute => entity[attribute]),
            ].join(',')
        }
        return null
    }


   /**
    * Return encoded *data* as JSON string.
    *
    * This will translate objects with type dayjs into string representation.
    * If time zone support is enabled on the server the date
    * will be sent as UTC, otherwise in local time.
    *
    * @param data The data to encode.
    * @return Encoded data
    */
    private encode(data: any): any {
        if (!data) {
            return data
        }

        if (isArray(data)) {
            return data.map(item => this.encode(item) as string)
        }

        if (isPlainObject(data)) {
            const out: RecordObject = {}
            forIn(data, (value, key) => {
                out[key] = this.encode(value)
            })

            return out
        }

        if (data && dayjs.isDayjs(data)) {
            const d = data as dayjs.Dayjs
            if (
                this.serverInformation &&
                this.serverInformation.is_timezone_support_enabled
            ) {
                // Ensure that the dayjs object is in UTC and format
                // to timezone naive string.
                return {
                    __type__: 'datetime',
                    value: d.utc().format(ENCODE_DATETIME_FORMAT),
                }
            }

            // Ensure that the dayjs object is in local time zone and format
            // to timezone naive string.
            return {
                __type__: 'datetime',
                value: data.local().format(ENCODE_DATETIME_FORMAT),
            }
        }

        return data
    }

    /**
     * Return error instance from *response*.
     */
    private getErrorFromResponse(type: string, msg: string, error_code?: string) {
        let ErrorClass

        if (type === 'ValidationError') {
            ErrorClass = ServerValidationError
        } else if (
            type === 'FTAuthenticationError' ||
            type === 'PermissionError'
        ) {
            ErrorClass = ServerPermissionDeniedError
        } else {
            ErrorClass = ServerError
        }

        const error = new ErrorClass(msg, error_code)

        return error
    }

   /**
    * Iterate *data* and decode entities with special encoding logic.
    *
    * Iterates recursively through objects and arrays.
    *
    * Will merge ftrack entities multiple occurrences which have been
    * de-duplicated in the back end and point them to a single object in
    * *identityMap*.
    *
    * datetime objects will be converted to timezone-aware dayjs objects.
    *
    * @param data The data to decode.
    * @return Decoded data
    */
    private decode(data: any, identityMap: RecordObject = {}): any {
        if (data == null) {
            return data
        } else if (isArray(data)) {
            return this._decodeArray(data, identityMap)
        } else if (isPlainObject(data)) {
            if (data.__entity_type__) {
                return this._mergeEntity(data, identityMap)
            } else if (data.__type__ === 'datetime') {
                return this._decodeDateTime(data)
            }
            return this._decodePlainObject(data, identityMap)
        }
        return data
    }

    /**
     * Decode datetime *data* into dayjs objects.
     *
     * Translate objects with __type__ equal to 'datetime' into dayjs
     * datetime objects. If time zone support is enabled on the server the date
     * will be assumed to be UTC and the dayjs will be in utc.
     */
    private _decodeDateTime(data: DatetimeObject) {
        if (
            this.serverInformation &&
            this.serverInformation.is_timezone_support_enabled
        ) {
            // Return date as dayjs object with UTC set to true.
            return dayjs.utc(data.value)
        }

        // Return date as local dayjs object.
        return dayjs(data.value)
    }

    /**
     * Return new object where all values have been decoded.
     */
    private _decodePlainObject(object: RecordObject, identityMap: RecordObject) {
        return Object.keys(object).reduce((previous, key) => {
            previous[key] = this.decode(object[key], identityMap)
            return previous
        }, {} as RecordObject)
    }

    /**
     * Return new Array where all items have been decoded.
     */
    private _decodeArray(collection: any[], identityMap: RecordObject): any[] {
        return collection.map(item => this.decode(item, identityMap))
    }

    /**
     * Return merged *entity* using *identityMap*.
     */
    private _mergeEntity(entity: Entity, identityMap: RecordObject) {
        const identifier = this.getIdentifyingKey(entity)
        if (!identifier) {
            logger('Identifier could not be determined for: ', identifier)
            return entity
        }

        if (!identityMap[identifier]) {
            identityMap[identifier] = {}
        }

        // Retrieve entity from identity map. Any instances which occur multiple
        // times in the encoded data will point to the same JavaScript object.
        // This means that output is not guaranteed to be JSON-serializable.
        //
        // TODO: Should we duplicate the information between the instances
        // instead of pointing them to the same instance?
        const mergedEntity = identityMap[identifier]

        forIn(
            entity,
            (value, key) => {
                mergedEntity[key] = this.decode(value, identityMap)
            }
        )

        return mergedEntity
    }

    /** Return encoded *operations*. */
    encodeOperations(operations: Operation[]) {
        return JSON.stringify(this.encode(operations))
    }

    /**
     * Call API with array of operation objects in *operations*.
     *
     * Returns promise which will be resolved with an array of decoded
     * responses.
     *
     * The return promise may be rejected with one of several errors:
     *
     * ServerValidationError
     *     Validation errors
     * ServerPermissionDeniedError
     *     Permission defined errors
     * ServerError
     *     Generic server errors or network issues
     *
     * @param operations - API operations.
     *
     */
    async call<T extends Array<any>>(operations: Operation[]): Promise<T> {
        const url = `${this.serverUrl}${this.apiEndpoint}`

        // Delay call until session is initialized if initialization is in
        // progress.
        if (this.initializing && !this.initialized) {
            await this.initializing
        }

        let response: Response

        try {
            response = await fetch(url, {
                method: 'post',
                credentials: 'include',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'ftrack-api-key': this.apiKey,
                    'ftrack-user': this.apiUser,
                    'ftrack-Clienttoken': this.clientToken,
                },
                body: this.encodeOperations(operations),
            })
        } catch (error: any) {
            logger('Failed to perform request. ', error)
            throw this.getErrorFromResponse('NetworkError', error.message)
        }

        let data: T | RecordObject
        try {
            data = this.decode(await response.json())
        } catch (error: any) {
            logger('Server reported error in unexpected format. ', error)
            throw this.getErrorFromResponse('MalformedResponseError', error.message)
        }

        if ((data as RecordObject).exception) {
            const data_ = data as RecordObject
            throw this.getErrorFromResponse(data_.exception, data_.content, data_.error_code)
        }

        return data as T
    }

    /**
    * Return promise of *entityType* with *data*, create or update if necessary.
    *
    *   *data* should be a dictionary of the same form passed to `create`
    *   method.
    *
    *   By default, check for an entity that has matching *data*. If
    *   *identifyingKeys* is specified as a list of keys then only consider the
    *   values from *data* for those keys when searching for existing entity.
    *
    *   If no *identifyingKeys* specified then use all of the keys from the
    *   passed *data*.
    *
    *   Raise an Error if no *identifyingKeys* can be determined.
    *
    *   If no matching entity found then create entity using supplied *data*.
    *
    *   If a matching entity is found, then update it if necessary with *data*.
    */
    async ensure<T extends Entity>(entityType: string, data: RecordObject, identifyingKeys: string[] = []) {
        let keys = identifyingKeys

        logger('Ensuring entity with data using identifying keys: ',
            entityType, data, identifyingKeys
        )

        if (!keys.length) {
            keys = Object.keys(data)
        }

        if (!keys.length) {
            throw new Error(
                'Could not determine any identifying data to check against ' +
                `when ensuring ${entityType} with data ${data}. ` +
                `Identifying keys: ${identifyingKeys}`
            )
        }

        const primaryKeys = this.getPrimaryKeyAttributes(entityType)!
        let expression = `select ${primaryKeys.join(', ')} from ${entityType} where`
        const criteria = keys.map(identifyingKey => {
            let value = data[identifyingKey]

            if (isString(value)) {
                value = `"${value}"`
            } else if (dayjs.isDayjs(dayjs)) {
                // Server does not store microsecond or timezone currently so
                // need to strip from query.
                value = (value as dayjs.Dayjs).utc().format(ENCODE_DATETIME_FORMAT)
                value = `"${value}"`
            }
            return `${identifyingKey} is ${value}`
        })

        expression = `${expression} ${criteria.join(' and ')}`

        const response = await this.query<T>(expression)
        if (response.data.length === 0) {
            return (await this.create<T>(entityType, data)).data
        }

        if (response.data.length !== 1) {
            throw new Error(
                'Expected single or no item to be found but got multiple ' +
                `when ensuring ${entityType} with data ${data}. ` +
                `Identifying keys: ${identifyingKeys}`
            )
        }

        const updateEntity = response.data[0] as RecordObject
        // Update entity if required.
        let updated = false
        Object.keys(data).forEach(key => {
            if (data[key] !== updateEntity[key]) {
                updateEntity[key] = data[key]
                updated = true
            }
        })

        if (updated) {
            return (await this.update<T>(
                entityType,
                primaryKeys.map(key => updateEntity[key]),
                Object.keys(data).reduce(
                    (accumulator, key) => {
                        if (primaryKeys.indexOf(key) === -1) {
                            accumulator[key] = data[key]
                        }
                        return accumulator
                    },
                    {} as Record<string, any>
                )
            )).data
        }

        return response.data[0]
    }

    /**
     * Return schema with id or null if not existing.
     * @param schemaId Id of schema model, e.g. `AssetVersion`.
     * @return Schema definition
     */
    getSchema(schemaId: string) {
        if (!this.schemas) {
            throw new Error('Schemas has not been retrieved')
        }

        for (const index in this.schemas) {
            if (this.schemas[index].id === schemaId) {
                return this.schemas[index]
            }
        }

        return null
    }

    /**
     * Perform a single query operation with *expression*.
     *
     * @param expression - API query expression.
     * @return Promise which will be resolved with an object
     * containing data and metadata
     */
    async query<T extends Entity>(expression: string) {
        logger('Query', expression)

        const operation = queryOperation(expression)
        const response = await this.call<[QueryResponse<T>]>([operation])

        return response[0]
    }

    /**
     * Perform a single create operation with *type* and *data*.
     *
     * @param type entity type name.
     * @param data data which should be used to populate attributes on the entity.
     */
    async create<T extends Entity>(type: string, data: RecordObject) {
        logger('Create', type, data)

        const response = await this.call<[CreateResponse<T>]>([createOperation(type, data)])

        return response[0]
    }

    /**
     * Perform a single update operation on *type* with *keys* and *data*.
     *
     * @param type Entity type
     * @param keys Identifying keys, typically [<entity id>]
     * @return Promise resolved with the response.
     */
    async update<T extends Entity>(type: string, keys: string[], data: RecordObject) {
        logger('Update', type, keys, data)

        const response = await this.call<[UpdateResponse<T>]>([updateOperation(type, keys, data)])

        return response[0]
    }

    /**
     * Perform a single delete operation.
     *
     * @param type Entity type
     * @param keys typically [<entity id>]
     */
    async delete(type: string, keys: string[]) {
        logger('Delete', type, keys)

        const response = await this.call<[DeleteOperation]>([deleteOperation(type, keys)])

        return response[0]
    }

    /**
     * Return an URL where *componentId* can be downloaded.
     *
     * @param componentId Is assumed to be present in the ftrack.server location.
     * @return URL where *componentId* can be downloaded, null if component id is not specified.
     */
    getComponentUrl(componentId?: string) {
        if (!componentId) {
            return null
        }

        const params = {
            id: componentId,
            username: this.apiUser,
            apiKey: this.apiKey,
        }

        return `${this.serverUrl}/component/get?${encodeUriParameters(params)}`
    }

    /**
     * Return an URL where a thumbnail for *componentId* can be downloaded.
     *
     * @param componentId - Is assumed to be present in the
     *                  ftrack.server location and be of a valid image file type.
     * @return URL where *componentId* can be downloaded. Returns the
     *                  URL to a default thumbnail if component id is not
     *                  specified.
     */
    thumbnailUrl(componentId?: string, options: ThumbnailOptions = {}) {
        const { size = 300 } = options
        if (!componentId) {
            return `${this.serverUrl}/img/thumbnail2.png`
        }

        const params = {
            id: componentId,
            size: size.toString(),
            username: this.apiUser,
            apiKey: this.apiKey,
        }

        return `${this.serverUrl}/component/thumbnail?${encodeUriParameters(params)}`
    }

    /**
     * Create component from *file* and add to server location.
     *
     * @param The file object to upload.
     * @param options Component data
     * @return Promise resolved with the response when creating
     * Component and ComponentLocation.
     */
    async createComponent(file: File, options: CreateComponentOptions = {}) {
        const normalizedFileName = normalizeString(file.name)
        const fileNameParts = splitFileExtension(normalizedFileName)
        const defaultProgress = (progress: number) => progress
        const defaultAbort = () => {}

        const data: Required<CreateComponentOptions>['data'] = options.data || {}
        const onProgress = options.onProgress || defaultProgress
        const xhr = options.xhr || new XMLHttpRequest()
        const onAborted = options.onAborted || defaultAbort

        const fileType = data.file_type || fileNameParts[1]
        const fileName = data.name || fileNameParts[0]
        const fileSize = data.size || file.size
        const componentId = data.id || uuidV4()
        const componentLocationId = uuidV4()

        const updateOnProgressCallback = (oEvent: ProgressEvent) => {
            if (oEvent.lengthComputable) {
                onProgress(Math.floor(oEvent.loaded / oEvent.total * 100))
            }
        }

        logger('Fetching upload metadata.')

        const uploadMetadata = (await this.call<[UploadMetadata]>([{
            action: 'get_upload_metadata',
            file_name: `${fileName}${fileType}`,
            file_size: fileSize,
            component_id: componentId,
        }]))[0]

        logger('Creating component and component location.')

        const component = Object.assign(data, {
            id: componentId,
            name: fileName,
            file_type: fileType,
            size: fileSize,
        })
        const componentLocation = {
            id: componentLocationId,
            component_id: componentId,
            resource_identifier: componentId,
            location_id: SERVER_LOCATION_ID,
        }

        const componentAndLocation = await this.call(
            [
                createOperation('FileComponent', component),
                createOperation('ComponentLocation', componentLocation),
            ]
        )

        logger('Uploading file to: %s', uploadMetadata.url)

        await new Promise((resolve, reject) => {
            // wait until file is uploaded
            xhr.upload.addEventListener('progress', updateOnProgressCallback)
            xhr.open('PUT', uploadMetadata.url, true)
            xhr.onabort = async () => {
                onAborted()
                await this.call([
                    deleteOperation('FileComponent', [componentId]),
                    deleteOperation('ComponentLocation', [componentLocationId]),
                ])

                reject(new CreateComponentError('Upload aborted by client', 'UPLOAD_ABORTED'))
            }

            Object.entries(uploadMetadata.headers)
                .filter(([k, _]) => k !== 'Content-Length')
                .forEach(([k, v]) => {
                    xhr.setRequestHeader(k, v)
                })

            xhr.onload = () => {
                if (xhr.status >= 400) {
                    reject(new CreateComponentError(`Failed to upload file: ${xhr.status}`))
                }
                resolve(xhr.response)
            }
            xhr.onerror = () => {
                reject(new CreateComponentError(`Failed to upload file: ${xhr.status}`))
            }
            xhr.send(file)
        })

        return componentAndLocation
    }

}

export default Session
