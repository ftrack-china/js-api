import { v4 as uuidV4 } from 'uuid'

export type EventSource = any & {
    id: string
    applicationId: string,
}

export interface EventData {
    id: string
    topic: string
    data: any

    target: string
    send?: string | null
    source?: EventSource

    inReplyToEvent?: string
}

// @ts-ignore: Duplicate identifier
export class Event {
    private _data: EventData

    /**
     * 
     * @param topic representing the event
     * @param data event payload
     * @param options 
     */
    constructor(topic: string, data: object, options?: object) {
        this._data = Object.assign(
            {
                topic,
                data,
                target: '',
            },
            options,
            {
                id: uuidV4(),
                sent: null,
            }
        )
    }

    getData() {
        return this._data
    }

    addSource(source: EventSource) {
        this._data.source = source
    }
}
